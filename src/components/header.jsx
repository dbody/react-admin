import React, { Component } from 'react';


export default class Header extends Component {
  render() {
    return (
		<header className="zk-header">
             <div className="toggle-box"><i className="fa fa-bars"></i></div>
             <a className="logo">React<span>Admin</span></a>
    	 </header>
    );
  }
}
