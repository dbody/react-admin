import React, { Component } from 'react';
// import logo from './assets/logo.svg';
import './styles/app.css';

import Header from './components/header';
import Aside from './components/aside';

export default class App extends Component {
  render() {
    return (
      <section className="zk-wrap">
          <Header />
          <Aside />
          <section className="zk-main-container">
               <div className="main-wrap">

               </div>
          </section>
      </section>
    );
  }
}
